# Midterm Exam

## Total

72/100

## Break Down

1. Data Types:                  17/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               2/5
    - Results:                  5/5
2. Selection:                   5/20
    - Compiles:                 5/5
    - Selection Structure:      0/10
    - Results:                  0/5
3. Repetition:                  20/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      16/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  1/5
5. Objects:                     14/20
    - Variables:                5/5
    - Constructors:             3/5
    - Accessors and Mutators:   3/5
    - toString and equals:      4/5

## Comments

1. Needed to convert to a `char` not a `String`.
2. Missing everything after the input prompt.
3. Good
4. Only works if the words occur sequentially.
5. Non-default constructor and mutator methods don't validate `age` and `equals` doesn't follow UML diagram and take an Object as a parameter.
