package exams.first;

public class Pet {


	private String name;//create private variables
	private int age;
	
	public Pet() {//create default constructor
		name= "Leo";
		age=3;
	}
	
	public Pet(String name, int age) {//create constructor with user input
		this.name=name;
		this.age=age;
	}
	
	public void setName(String name) {//set value of name
		this.name=name;
	}
	
	public void setAge(int age) {//set value of age
		this.age=age;
	}
	
	public String getName() {//assign value to name
		return name;
	}
	
	public int getAge() {//assign value to age
		return age;
	}
	
	public boolean equals(Pet obj) {//compare two constructors
		if ((this.name.equals(getName())) && (this.age == getAge())){
			return true;
		}else {
			return false;
		}
	}
	
	public String toString() {//create string that is easy to access
		return(name+ "is " + age + "years old");
	}
}
