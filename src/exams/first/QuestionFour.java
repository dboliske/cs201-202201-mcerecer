package exams.first;

import java.util.Scanner;

public class QuestionFour {

	public static void main(String[] args) {
	  
		Scanner input = new Scanner(System.in);//open scanner

        String[] words = new String[5];//create array "words "
        
        for (int i = 0; i < words.length; i++) {//Prompts user for words until array is full
            System.out.println("Enter a word:");
            words[i] = input.nextLine();
        }
        
        for (int n = 0; n < 4; n++) {//compares strings in array
        	if(words[n].equals(words[n+1])){
        		System.out.println(words[n]);
        	}
        }
        input.close();
	}

}
