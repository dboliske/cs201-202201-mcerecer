package exams.second;

public class Circle extends Polygon{

	double radius;
	
	public Circle() {
		radius = 1.5;
	}
	
	public void setRadius(double width) {
		if (width > 1) {
			radius = width;
		}else {
			radius = 1;
		}
	}
	
	public double area() {
		return Math.PI*radius*radius;
	}
	
	public double perimeter() {
		return 2.0*Math.PI*radius;
	}
}
