package exams.second;

public class ComputerLab extends Classroom{

	boolean computers;
	
	public ComputerLab() {
		building = super.getBuilding();
		roomNumber = super.getRoomNumber();
		seats = super.getSeats();
		computers= true;
	}
	
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		return computers;
	}
	
	@Override
	public String toString() {
		if(computers == true) {
		return "Classroom is on building " + super.building + ", in room " + super.roomNumber + ",and has " + super.seats + "seats and has computers";
		}else {
			return "Classroom is on building " + super.building + ", in room " + super.roomNumber + ",and has " + super.seats + "seats and does not has computers";
		}
	}
}
