package exams.second;

public class Classroom {
	
	String building = null;
	String roomNumber = null;
	int seats;

	public Classroom() {
		building = "Siege Hall";
		roomNumber = "SH103";
		seats = 25;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String number) {
		roomNumber = number;
	}
	
	public void setSeats(int seats) {
		if (seats < 0) {
		this.seats = seats;
		}
	}
	
	public String getBuilding() {
	return building;
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}
	
	@Override
	public String toString() {
		return "Classroom is on building " + building + ", in room " + roomNumber + ",and has " + seats + "seats";
	}
	
	
}


