package exams.second;

public class Rectangle extends Polygon{

	double width;
	double height;
	
	public Rectangle() {
		width = 10;
		height = 16;
	}
	
	public void setWidth(double width) {
		if (width > 1) {
			this.width = width;
		}else {
			this.width = 1;
		}
	}
	
	public void setHeight(double height) {
		if (height > 1) {
			this.height = height;
		}else {
			this.height = 1;
		}
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public String toString() {
		return  super.getName()+ " is a rectangle ";
	}
	
	public double area() {
		return width*height;
	}
	
	public double perimeter() {
		return 2.0*(height+width);
	}
	
}
