package exams.second;

public class Polygon {

	String name = null;
	
	public Polygon() {
		name = "hexagon";
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "The polygon is a " + name;
	}
	
	public double area() {
		
		if(name == "rectangle") {
			Rectangle a = new Rectangle();
			double areaR = a.area();
			return areaR;
		}
		
		if(name == "circle") {
			Circle b = new Circle();
			double areaC = b.area();
			return areaC;
		}
		return 0.0;
	
		}
	
	public double perimeter() {

		if(name == "rectangle") {
			Rectangle a = new Rectangle();
			double periemterR = a.perimeter();
			return periemterR;
		}
		
		if(name == "circle") {
			Circle b = new Circle();
			double perimeterC = b.perimeter();
			return perimeterC;
		}
		return 0.0;
	
	}
}
