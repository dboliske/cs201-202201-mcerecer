package exams.second;

public class Sorting {
	public  static void main(String[] args) {
		
	String list[] = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
	int n =list.length;
	
	System.out.println("Given array is");
	
	for(int i = 0; i < n; i++) {
		System.out.print(i+1 + ": " + list[i] + " ");
	}
	System.out.println();
	
	selectionSort(list,n);
	
	System.out.println("Sorted array is");
	for(int i = 0; i < n; i++) {
		System.out.print(i+1 + ":" + list[i] + " ");
	}
	
	}
	public static void selectionSort(String list[], int n) {

		for(int i = 0; i<list.length-1 ; i++) {
			int min = i;
			String minString = list[i];
			
			for(int j = i+1; j<list.length; j++) {
				if(list[j].compareTo(minString) < 0) {
					minString = list[j];
					min = j;
				}
			}
			
			if (min != i) {
				String temp = list[min];
				list[min] = list[i];
				list[i] = temp;
				
			}
		}
	}
}
