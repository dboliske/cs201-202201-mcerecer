package exams.second;

import java.util.*;

public class Searching {
	public  static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		double list[] = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		System.out.print("Which number are you searching");
		double x = input.nextDouble();
		
		int index = jumpSearch(list,x);
		System.out.println("Number located on index " + index);
		
		input.close();
	}
	
	public static int jumpSearch(double [] list , double x) {
	
		int n = list.length;
		int step =(int)Math.floor(Math.sqrt(n));
		int prev = 0;
		
		while(list[Math.min(step, n)-1] < x) {
			prev = step;
			step += (int)Math.floor(Math.sqrt(n));
			if(prev >= n) {
				return -1;
			}
		}
		while(list[prev] < x) {
			prev++;
			if(prev == Math.min(step, n)) {
				return -1;
			}
			
		}
		if(list[prev] == x) {
			return prev;
		}
		
		return -1;
	}
}
