package project;
//Main class of the program, here will be the methods that will help run the program, and will use other classes to create,modify and erase user-defined objects and will read and save files
//Mario Cerecer
//04/29/2022

import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StoreMenu {

		

	Scanner input = new Scanner(System.in);
	
	public static ArrayList<ShelvedItem> Menu(Scanner input, ArrayList<ShelvedItem> data) {//Show options and call methods depending on the response
		String choice = null;
		ArrayList<ShelvedItem> cart = new ArrayList<ShelvedItem>(1);
		
		do {
		
		System.out.println("Choose one of the following options (enter number):");
		System.out.println("1) Add item to stock");
		System.out.println("2) Add item to cart");
		System.out.println("3) Search Item");
		System.out.println("4) Modify Item");
		System.out.println("5) Save");
		System.out.println("6) Check cart");
		System.out.println("7) Check stocks");
		System.out.println("8) Checkout");
		System.out.println("9) Exit");
		choice = input.nextLine();
		
			switch (choice) {
			case "1" ://Call addItem method
				data = addItem(data,input);
				break;
			
			case "2" ://Will search the index where item is located, remove it from the stock array, and add it to the cart array
				try {
				System.out.println("Which item you want to add");
				String sell = input.nextLine();
				
				int toCart = search(data,sell);
				cart.add(data.get(toCart));
				
				data = removeItem(data,sell);
				break;
				}catch(Exception e) {
					System.out.println("Item not found. Returning to menu.");
					break;
				}
			
			case "3" ://Search item index and print it
				try {
				System.out.println("Which item you want to search");
				String locate = input.nextLine();
				int location =search(data, locate);
				if (location == -1) {
					System.out.println("Item not found");
				}else {
				System.out.println("item located on index " + location);
				}
				break;
				}catch(Exception e) {
					System.out.println("Item not found. Returning to menu.");
					break;
				}
				
			case "4"://Search on which index the item is located and then use methods to modify the items
				System.out.println("Which item you want to modify");
				String itemName = input.nextLine();
				modifyItem(input, data, cart, itemName);
				break;
		
			case "5" ://Prompt user for the name of the file and create a new file with all the current stock
				System.out.println("How do you want to call the file");
				String filename = input.nextLine();
				saveFile(filename, data);
				break;
				
			case "6" : // Print the current cart
				System.out.println(cart);
				break;
				
			case "7" : // Print the current stock
				System.out.println(data);
				break;
				
			case "8"://Erase elements of cart and add the price
				double total = checkout(cart);
				System.out.println(total);
				break;
				
			case "9" ://Close program
				return cart;
				
			default ://What to do in case the user enter something else
				System.out.println("I'm sorry, but I didn't understand that.");
				break;
			}
	
		}while (choice != "8");
			
		return data;
	}
	
	public static double checkout (ArrayList<ShelvedItem> cart) {//Erase all items in the cart and use getPrice to calculate the total price
		
		double total = 0 ;
		for(int i = 0; i < cart.size(); i++) {
			total = total + cart.get(i).getPrice();
			cart.remove(i);
		}
		return total;
	}
	public static void modifyItem (Scanner input, ArrayList<ShelvedItem> data ,ArrayList<ShelvedItem> cart, String itemName){//Search for all the index of the item and ask what to modify, then use methods to modify and get the new values
		
   	ArrayList<Integer> indexListData = new ArrayList<Integer>(1);
	indexListData = modifySearch(data,itemName);
	System.out.println(indexListData);
	
	ArrayList<Integer> indexListCart = new ArrayList<Integer>(1);
	indexListCart = modifySearch(cart, itemName);
	System.out.println(indexListCart);
	
	System.out.print("What do you want to modify (name or price)");
	String choice = input.nextLine();
	
	if(choice.equalsIgnoreCase("name")) {
		System.out.print("Enter new name");
		String newName = input.nextLine();		
	for(int i = 0; i<indexListData.size(); i++) {
		int index = indexListData.get(i);
		data.get(index).setName(newName);
		data.get(index).getName();
	}
	for(int i = 0; i<indexListCart.size(); i++) {
		int index = indexListCart.get(i);
		cart.get(index).setName(newName);
		cart.get(index).getName();
	}
	}else if(choice.equalsIgnoreCase("price")){
		System.out.print("Enter new price");
		double newPrice = input.nextDouble();
	for(int i = 0; i<indexListData.size(); i++) {
		int index = indexListData.get(i);
		data.get(index).setPrice(newPrice);
		data.get(index).getPrice();
	}
	for(int i = 0; i<indexListCart.size(); i++) {
		int index = indexListCart.get(i);
		cart.get(index).setPrice(newPrice);
		cart.get(index).getPrice();
	}
	}

	}
	
	public static ArrayList<Integer>  modifySearch (ArrayList<ShelvedItem> data ,String value) {//Search for all instances of the item and create array with all the indexes found
		
		ArrayList<Integer> indexList = new ArrayList<Integer>(1);
	
			for(int i = 0; i<data.size(); i++) {
				if(data.get(i).getName().equalsIgnoreCase(value)) {
					indexList.add(i);
				} 
			}
			return indexList;
		}
	
	
	public static ArrayList<ShelvedItem> addItem (ArrayList<ShelvedItem> data , Scanner input) {//Prompt for name, price and type of item and then use constructors to create items
		
		ShelvedItem a = null;
		System.out.println("Item name");
		String name = input.nextLine();
		System.out.println("Item price");
		double price = 0.0;
		try {
			price = input.nextDouble();
		}catch (Exception e) {
			System.out.println("That is not a valid price. Returning to menu.");
			return data;
		}
		
		System.out.println("Type of Item (Shelved, Produce, or AgeRestricted): ");
		String itemtype = null;
		itemtype = input.nextLine();
		itemtype = input.nextLine();
		switch(itemtype) {
		case "shelved":
			a = new ShelvedItem(name,price);
			break;
		case "agerestricted":
			System.out.println("Enter requited age");
			int age = input.nextInt();
			a = new AgeRestrictedItem(name,price,age);
			break;
		case "produce":
			System.out.println("Enter expiration date");
			String expDate = input.nextLine();
			a = new ProduceItem(name,price,expDate);
			break;
			}
			
		data.add(a);
		return data;
		}
	
	public static ArrayList<ShelvedItem> removeItem(ArrayList<ShelvedItem> data ,String value) {//Search the index where object is located and erase it
		
		int target = search(data, value);
		data.remove(target);
		return data;
		
	}
	
	public static int  search (ArrayList<ShelvedItem> data ,String value) {//Go through the array and find the index where the item is located (the last one found)
	
		int index = -1;

			for(int i = 0; i<data.size(); i++) {
				if(data.get(i).getName().equalsIgnoreCase(value)) {
					index = i;
				} 
			}
			return index;
		}
	
	public static void saveFile(String filename, ArrayList<ShelvedItem> data) {//Save current stock in a csv file with a name given by the user
		
		try {
			File file = new File(filename);
		      
		    file.createNewFile();
		    Path p = Paths.get(filename);
		    
			FileWriter writer = new FileWriter(file);
			System.out.println(p.toAbsolutePath().toString());
			for (int i=0; i<data.size(); i++) {
				writer.write(data.get(i).toCSV() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error saving to file.");
		}
	}
	
	public static ArrayList<ShelvedItem> readFile (String filename) {//Read file and create the initial stock of the store
		
		ArrayList<ShelvedItem> fileData = new ArrayList<ShelvedItem>(5);
		
		try {
			filename = "C:\\Users\\mario\\eclipse-workspace\\CS201 work\\src\\project\\stock.csv";
			File f = new File (filename);
			Scanner input = new Scanner(f);
			
			while(input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					ShelvedItem a = null;
					
					if(values.length == 2) {
						a = new ShelvedItem(values[0], Double.parseDouble(values[1]));
						fileData.add(a);
					}else if(values.length == 3) {
						int dataType = values[2].length();
						if(dataType<4) {
							a = new AgeRestrictedItem(values[0], Double.parseDouble(values[1]), Integer.parseInt(values[2]));
							fileData.add(a);
						}else{
							a = new ProduceItem(values[0], Double.parseDouble(values[1]), values[2]);
							fileData.add(a);
						}
					}
				} catch (Exception e) {
					//do nothing
				}
			}
		}catch (FileNotFoundException fnf) {
			// do nothing.
		} catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		return fileData;
	}
	
	public  static void main(String[] args) {
		
		ArrayList<ShelvedItem> data = new ArrayList<ShelvedItem>(5);
		ArrayList<ShelvedItem> dataFile = new ArrayList<ShelvedItem>(5);
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter file to load");
		String filename = input.nextLine();
		dataFile = readFile(filename);
		
		data = Menu(input, dataFile);
		
		System.out.println("Program ended");
	}
		
}


