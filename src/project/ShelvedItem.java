package project;
//Class that will create shelved items (the most basic item in store), and is the superclass of the produce and age restricted item
//Mario Cerecer
//04/29/2022
public class ShelvedItem{

	
	private String name;
	private double price;
	
	public ShelvedItem (String name, double price){//Constructor to create a shelved item
		this.name = name;
		this.price = price;
	}
	
	public void setName(String name) {//Method to modify the name of the object 
		this.name = name;
	}
	
	public void setPrice(double price) {//Method to modify the price of the object
		this.price = price;
	}
	
	public String getName() {//Method to return the name of the object to the main class
		return name;
	}
	
	public double getPrice() {//Method to return the price of the object to the main class
		return price;
	}
	
	@Override
	public String toString() {//Method which will create the string that is added to the arraylist of items
		return "Shelved Item" + " - " + name + " - $" + price ;
	}
	
	protected String csvData() {//Create a string with name and price
		return name + "," + price;
	}
	
	public String toCSV() {//Create a string that will go in the save file
		return "Shelved Item," + csvData();
	}
	
	
	
}
