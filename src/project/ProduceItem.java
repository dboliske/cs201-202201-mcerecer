package project;
//Class that will create produce items, subclass of shelved item
//Mario Cerecer
//04/29/2022
public class ProduceItem extends ShelvedItem {

	private String expDate;
	
	public ProduceItem(String name, double price, String expDate){//Constructor to create a produce item
		super(name, price);
		this.expDate = expDate;
	}
	
	public void setExpDate(String expDate) {//Method to modify the expiration date of the object 
		this.expDate = expDate;
	}
	
	public String getExpDate() {//Method to return the expiration date of the object to the main class
		return expDate;
	}
	
	@Override
	public String toString () {//Method which will create the string that is added to the arraylist of items
		return "Produce Item" + " - " + super.getName() + " - $" +super.getPrice() + " - " + expDate;
	}
	
	@Override
	public String toCSV() {//Create a string that will go in the save file
		return "Produce Item," + super.csvData() + "," + expDate;
	}
}

