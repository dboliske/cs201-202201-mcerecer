package project;
//Class that will help create age rstricted items, subclass of shevled item
//Mario Cerecer
//04/29/2022
import java.io.*;

public class AgeRestrictedItem extends ShelvedItem{
	
	public int age;
	
	public AgeRestrictedItem (String name, double price, int age) {//Constructor to create an age restricted item
		super(name, price);
		this.age = age;
	}
	
	public void setAge(int age) {//Method to modify the required age of the object 
		this.age = age;
	}
	
	public int getAge() {//Method to return the get of the object to the main class
		return age;
	}
	
	@Override
	public String toString () {//Method which will create the string that is added to the arraylist of items
		return "Age Restricted Item" + " - " + super.getName() + " - $" + super.getPrice() + " - " + age;
	}
	
	@Override
	public String toCSV() {//Create a string that will go in the save file
		return "Age Restricted Item," + super.csvData() + "," + age;
	}

}
