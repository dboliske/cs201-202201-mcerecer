package labs.lab3;
//Mario Cerecer
//CS201
//Section 1
//02-09-2021
//Average
//This read the file "grade.csv" and uses the data to obtain the average
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class average {

	public static void main(String[] args) throws FileNotFoundException {

		File f = new File("src/labs/lab3/grades.csv");//Read values in external file
		Scanner input = new Scanner(f);
		
		double ave= 0;
		int t= 0;
		int [] grades= new int[14];//Create array that will sotre the values


		while(input.hasNextLine()) {//Create while loop
		String[] values= input.nextLine().split(",");//Split a string into two
		grades[t]= Integer.parseInt(values[1]);//Add values in the external file

		ave = grades[t]+ave;
		t++;

		}
		ave= ave/t;//Calculate values
		System.out.println("The average is " +ave);
		input.close();
	}
	

}