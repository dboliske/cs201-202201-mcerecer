package labs.lab3;
//Mario Cerecer
//CS201
//Section 1
//02-09-2021
//Average
//This program find the minimum value of a list of values 
public class exercise3min {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Create array given in instructions
		int[] lst = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		int min = lst[0];//Create value that will store the minimum value
		
		for(int i = 0; i<lst.length;i++) {//Create a for loop that finds the minimum value by comparing each value
			if(lst[i]<min){
				min = lst[i];}//Set minimum value to a variable
		}
		System.out.println("The minimum is " + min);

	}

}
