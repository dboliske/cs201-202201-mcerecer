package labs.lab3;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
//Mario Cerecer
//CS201
//Section 1
//02-09-2021
//Average
//This program promps the user for values and then stroes it in a new file
public class NewFile {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		
		System.out.println("Please enter in a grade (Enter Done to end):");//Ask the user to input a value
		String input = in.nextLine();
		
		ArrayList<Integer> lst = new ArrayList<Integer>();//Create array where values will be stored
		
		while(!input.equals("Done")) {//Creates a loop until "Done" is introduced to the console
			
			lst.add(Integer.parseInt(input));//Add value entered
			
			System.out.println("Please enter in a grade (Enter Done to end):");
			input = in.nextLine();
		}
		
		System.out.println("Enter in the file name: ");//Ask the name of the file where values will be stored
		input = in.nextLine();
		
		FileWriter f = new FileWriter("src/labs/lab3/"+input);//Create new file 
		
		for(int i=0; i<lst.size(); i++) {//Add values to the new file
			f.write(lst.get(i)+"\n");
		}
		f.close();
		in.close();

	}

}
