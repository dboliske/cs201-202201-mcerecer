package labs.lab1;
//Mario Cerecer
//CS201
//Section 1
//01-24-2021
//Inches to centimeters
import java.util.Scanner;

public class inTOcm {
	public static void main(String[] args) {
		//Test Table 
		//		Input		/		Expected Result		/		Given Result
		//		4			/		10.16				/		10.16
		//		9.72		/		24.6888				/		24.6888
		//		15.90		/		40.386				/		40.386
		//		23.54		/		59.7916				/		59.79159
		//		77.762		/		197.51548			/		187.51548
		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter your height in inches ");
		double height = Double.parseDouble(input.nextLine());
		
		System.out.println("Your height in cms is " +(height*2.54));	
	}

}
