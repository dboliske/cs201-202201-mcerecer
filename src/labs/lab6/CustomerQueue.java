package labs.lab6;
//Mario Cerecer
//CS201
//Section 1
//03/23/2022
//Customer Queue
//Create a program that show a menu with 3 option. First option add name to arraylist, the second print and delete the first name, and third exit the program
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class CustomerQueue {
	
	public static ArrayList<String> customers = new ArrayList<String>();//create arraylist

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);//create scanner

		int choice = 0;//create variable 
		
		try {
			
		while (choice != 3){//condition for while loop
			
		System.out.println("(1)Add customer to queue ");//print menu
		System.out.println("(2)Help customer ");
		System.out.println("(3)Exit");
		
		choice = input.nextInt();//input the user choice
		
		if(choice == 1) {//case 1
			System.out.println("Enter name");//ask user for input
			String name = input.next();//read next line 
			CASE1(name);//call method
			
		}else if (choice == 2) {//case 2
	
			CASE2();//call method
		}
		
		}
		}catch (Exception e) {//what to do in case of error
			
			System.out.println("Error");
			
		}
		
		input.close();//close input
		System.out.println(customers);//print arraylist
		
	}
	
	
	public static void CASE1 (String name) {//method for case 1
		customers.add(name);
	}
	
	public static void CASE2 () {
		System.out.println(customers.get(0));//method for case 2
		customers.remove(0);
	}
}
