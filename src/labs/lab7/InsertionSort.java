package labs.lab7;
//Mario Cerecer
//CS201
//Section 1
//04/04/2022
//InsertionSort
//Create a program that uses a insertion sort in a array of strings
public class InsertionSort {


	public static void main(String[] args) {
		
		String[] data = {"cat", "fat", "dog", "apple", "bat", "egg"};//array of fiven strings
		
		data=sort(data);//call method

		for (int k=0 ; k < data.length; k++) {//print array
			System.out.print(" " + data[k]);
		}
}
	
	public static String[] sort(String[] input) {
		
		for(int j = 0; j < input.length - 1; j++) {//condition for loop
			int i = j;
			while(i>0 && input[i].compareTo(input[i-1]) < 0) {//select elements to swap
				String temp = input[i];//swap elements
				input[i] = input[i-1];
				input[i-1] = temp;
				i--;
			}
		}
		return input;
	}
}
