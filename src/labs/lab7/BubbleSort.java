package labs.lab7;
//Mario Cerecer
//CS201
//Section 1
//04/04/2022
//BubbleSort
//Create a program that uses a bubble sort in a array of integers
public class BubbleSort {

	public static void main(String[] args) {
		
		int[] data = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};//data given
		
		data=sort(data);

		for (int k=0 ; k < data.length; k++) {//print array
			System.out.print(data[k]);
		}
}
	
	public static int[] sort(int[] input) {

		boolean swapped;
		
		do {
			swapped = true;
			for (int i = 0; i<input.length - 1; i++ ) {//swap elements condition
				if (input[i+1] < input[i]) {//select which elements to swap
					int temp = input[i+1];//swap elements
					input[i+1] = input[i];
					input[i] = temp;
					swapped = false;
					
				}
			}
		} while (!swapped);
		
		return input;
	}
}

