package labs.lab7;
//Mario Cerecer
//CS201
//Section 1
//04/04/2022
//Selection Sort
//Create a program that uses a selection sort in a array of doubles
public class SelectionSort {



	public static void main(String[] args) {
		
		double[] data = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};//array of given doubles
		
		data=sort(data);

		for (int k=0 ; k < data.length; k++) {//print array
			System.out.print(" " + data[k]);
		}
}
	
	public static double[] sort(double[] input) {
		
		for(int i = 0 ; i<input.length - 1 ; i++) {//create to variables that will never be the same to compare the elements
			int min = i;
			for(int j = i+1 ; j<input.length; j++) {
				if (input[j] < input[min]) {//if the element need to be swapped the program will assign another value to min
					min = j;
				}
			}
			if(min != i) {// if a new min was found it will swap elements
				double temp = input[min];
				input[min] = input[i];
				input[i] = temp;
			}
		}
		
		return input;
	}
	
}
