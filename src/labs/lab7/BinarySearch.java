package labs.lab7;
//Mario Cerecer
//CS201
//Section 1
//04/04/2022
//BinarySearch
//Create a program that uses binary search to find an element in array of strings
import java.math.*;
import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch {

	
	
		public static void main(String[] args) {
			
			String[] data = {"c", "html", "java", "python", "ruby", "scala"};// create array of given strings
			
			Scanner input = new Scanner(System.in);//choose word to find
			System.out.print("Search term: ");
			String value = input.nextLine();
			
			search(data, value);//call metod

		}

		public static void search(String[] parameter, String user) {
			
			int start=0;
			int finish= parameter.length - 1;
			double middle=(start+finish)/2;
			int upper=(user.compareToIgnoreCase(parameter[(int)middle+1]));//if middle is not an integer find the closest bigger integer
			int lower=(user.compareToIgnoreCase(parameter[(int)middle-1]));//if middle is not an integer find the closest smaller integer
			
			if(middle == Math.floor(middle)) {
				if (parameter[(int) middle].equalsIgnoreCase(user)) {//exactly on the middle
					System.out.println(user + "is found on index " + middle);
					return;
				}else if (upper == 0) {//check to see if the neighboring top is the value
					System.out.println(user + " is found on index " + (middle+1));
					return;
					
				}else if (lower == 0) {//check to see if the neighboring bot is the value
					System.out.println(user + " is found on index " + (middle-1));
					return;
				}else if (upper < lower) {//check to see if value is closer in the top half
					search(Arrays.copyOfRange(parameter, (int)middle+1, parameter.length), user);
				}else if (lower < upper) {//check to see if the value is closer in the bot half
					search(Arrays.copyOfRange(parameter, 0, (int)middle-1), user);
				}
			}
			
		}
		
	}
	
