package labs.lab4;
//Mario Cerecer
//CS201
//Section 1
//02-21-2021
//Phone Number
//Create constructor that have the values of coutryCode, areaCode, number and validate the data with given parameters
// and compare the default constructor with a personalized one
public class PhoneNumber {

private String countryCode;//create private variables
private String areaCode;
private String number;

	public PhoneNumber() {//assign default PhoneNumber values
		countryCode= "+52 ";
		areaCode= "33 ";
		number= "14497097";
	}

	public PhoneNumber(String cCode, String aCode, String number) {//Let user choose values on PhoneNumber
		countryCode = cCode;
		areaCode = aCode;
		this.number = number;
	}
	public String getCountryCode() {//Get value countryCode
		return countryCode;
	}
	public String getAreaCode() {//Get value AreaCode
		return areaCode;
	}
	public String getNumber() {//Get value Number
		return number;
	}
	public void setCountryCode(String cCode) {//Assign value to countryCode
		countryCode=cCode;
	}
	public void setAreaCode(String aCode) {//Assign value to AreaCode
		areaCode= aCode;
	}
	public void setNumber(String number) {//Assign value to number
		this.number = number;		
	}
	public String toString() {//Create a string I can easily repeat
		return countryCode + areaCode + number;
	}
	
	public boolean validAreaCode(String aCode) {//Validate value of areaCode
		if (aCode.length()==3) {
			return true;
		}else {
			return false;
		}
	}
	public boolean validNumber(String number) {//Validate value of number
		if (number.length()==7) {
			return true;
		}else {
			return false;
		}
	}
	public boolean equals(PhoneNumber pn) {//Compare two "PhoneNumbers" constructors
		if(this.countryCode != pn.getCountryCode()){
			return false;
		}
		else if (this.areaCode != pn.getAreaCode()){
			return false;
		}
		else if( this.number != pn.getNumber()) {
			return false;
		}
		return true;
	}
}
