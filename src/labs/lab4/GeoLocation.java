package labs.lab4;
//Mario Cerecer
//CS201
//Section 1
//02-21-2021
//GeoLocation
//Create constructor with values lat and lng, and use accessor/mutator methods to modify the values,
//validate the data with given parameters and compare constructors
public class GeoLocation {
	
	private double lat;//Create private variables
	private double lng;
	
	public GeoLocation() {//Assign values in GeoLocation
		lat= 41.87;
		lng= -87.62;
	}
	public GeoLocation(double la, double ln) {//Let user assign values in GeoLocation
		lat= la;
		lng= ln;
	}

	public void setLat(double lat) {//Set value of lat
		this.lat= lat;
	}
	public void setLng(double lng) {//Set value of lng
		this.lng=lng;
	}
	public double getLat() {// Assign value lat
		return lat;
	}
	public double getLng() {//Assign value to lng
		return lng;
	}
	
	public String toString() {//Create string I can easily repeat
		return "(" + lat + "," + lng + ")";
	}
	
	public boolean validLat (double lat) {//Validate lat based on given parameters
		if (lat >= -90 && lat <= 90) {
			return true;
		}else {
			return false;
		}
	}
	public boolean validLng (double lng) {//Validate lng based on given parameters 
		if (lng >= -180 && lng <= 180) {
			return true;
		}else {
			return false;
		}
	}
	public boolean equals(GeoLocation g) {//Compare constructors
		if(this.lat != g.getLat()){
			return false;
		}
		else if (this.lng != g.getLng()){
			return false;
		}
		return true;
	}
	
}
