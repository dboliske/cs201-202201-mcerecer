package labs.lab4;
//Mario Cerecer
//CS201
//Section 1
//02-21-2021
//Potion
//Create constructor with name and strength values, use accessor/mutator methods to modify data, validate
// with given parameters and compare constructors
public class Potion {
	private String name;//Create private variables
	private double strength;
	
	public Potion() {//Create default constructor
		name= "Carroll Shelby ";
		strength=17.2;
	}

	public Potion(String name, double strength) {//Create personalized constructor
		this.name= name;
		this.strength = strength;
	}
	
	public String getName() {// Get value of name
		return name;
	}
	public double getStrength() {// Get value of strength
		return strength;
	}
	public void setName(String name) {//Assign value to name
		this.name = name;
	}
	public void setStrength(double strength) {//Assign value to strength
		this.strength= strength;
	}
	public String toString() {// Create string easily to repeat
		return name + "strength is " + strength;
	}
	public boolean validStrength(double strength) {//Validate value of strength based on given parameters
		if(strength <=10 && strength >= 0) {
			return true;
		}else {
			return false;
		}
	}
	public boolean equals(Potion p) {//Compare constructors
		if(this.name != p.getName()) {
			return false;
		} else if(this.strength != p.getStrength()){
				return false;
			}
		return true;
		}
	}