package labs.lab4;
//Mario Cerecer
//CS201
//Section 1
//02-21-2021
//Phone Number
//Create a client that will use the PhoneNumber.java file
public class PhoneNumberClient {

	public static void main(String[] args) {
	
		PhoneNumber p1 = new PhoneNumber();//Print default constructor
		System.out.println(p1.toString());
		
		PhoneNumber p2 = new PhoneNumber("+52 ", "33 ", "14497097");//Print constructor with given data
		System.out.println(p2.toString());
		
		System.out.println(p2.validAreaCode(p2.getAreaCode()));//Validate AreaCode
		System.out.println(p1.validNumber(p2.getNumber()));//Validate number
		System.out.println(p1.equals(p2));//Compare constructor p1 and p2
		
	}

}
