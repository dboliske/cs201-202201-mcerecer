package labs.lab4;
//Mario Cerecer
//CS201
//Section 1
//02-21-2021
//Phone Number
//Create a client that will use the Potion.java file
public class PotionClient {

	public static void main(String[] args) {

		Potion p1 = new Potion();//Print default constructor
		System.out.println(p1.toString());
		
		Potion p2 = new Potion("John", 9.4);//Print constructor with given data
		System.out.println(p2.toString());
		
		System.out.println(p1.validStrength(p2.getStrength()));//Validate data in the constructor
		System.out.println(p1.equals(p2));//Compare constructor p1 and p2
	}

}
