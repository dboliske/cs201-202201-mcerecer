package labs.lab4;
//Mario Cerecer
//CS201
//Section 1
//02-21-2021
//Phone Number
//Create a client that will use the GeoLocation.java file
public class GeoLocationClient {

	public static void main(String[] args) {

		GeoLocation g1= new GeoLocation();//Print default constructor
		System.out.println(g1.toString());
		g1.setLat(80);//Modify lat with a mutator method
		System.out.println(g1);
		g1.setLng(-95);//Modify lng with a mutator method
		System.out.println(g1);
		
		
		GeoLocation g2= new GeoLocation(53.86, -97.2);//Print personalized constructor
		System.out.println(g2.toString());
		System.out.println(g1.equals(g2));//Compare constructor g1 and g2
		System.out.println(g2.validLat(g2.getLat()));//Validate lat
		System.out.println(g2.validLng(g2.getLng()));//Validate lng
		

	}
	

}
