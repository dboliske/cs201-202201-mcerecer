package labs.lab5;

public class GeoLocation {

	protected double lat;
	protected double lng;
	
	public GeoLocation() {
		lat=41.86737;
		lng=-87.6274;
	}
	
	public GeoLocation(double lat, double lng) {
		this.lat=lat;
		this.lng=lng;
	}
	
	public double getLat() {
		return lat;
	}
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) {
		this.lat=lat;	
	}
	public void setLng(double lng) {
		this.lng=lng;
	}
	
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}
	
	public boolean validLat (double lat) {
		if (lat >= -90 && lat <= 90) {
			return true;
		}else {
			return false;
		}
	}
		
	public boolean validLng (double lng) {
		if (lng >= -180 && lng <= 180) {
			return true;
		}else {
			return false;
		}
	}	
		
	public boolean equals(GeoLocation g) {
		if(this.lat != g.getLat()){
			return false;
		}
		else if (this.lng != g.getLng()){
			return false;
		}
		return true;
	}
	
	public double calcDistance(GeoLocation g) {
		return  Math.sqrt(Math.pow(this.lat-g.getLat(),2)+Math.pow(this.lng-g.getLng(),2));
	}
	
	public double calcDistance(double lat, double lng) {
		return  Math.sqrt(Math.pow(this.lat-lat,2)+Math.pow(this.lng-lng,2));
	}
}
