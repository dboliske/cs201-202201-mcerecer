package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;
import labs.lab5.CTAStation;
import java.util.*;

public class CTAStopApp{
	
	static CTAStation[] CTAcollection= new CTAStation[1];

	public static void main(String[] args) {
		CTAStation[] stations = readFile();
		menu(stations);
	}
	
	public static  CTAStation[] readFile(){
		
		try {
		Scanner input = new Scanner(new File("C:\\Users\\mario\\git\\cs201-202201-mcerecer\\src\\labs\\lab5\\CTAStops.csv"));
		

		int count=0;
		
		while (input.hasNextLine()){
			input.nextLine();
			count++;
		}
		input.close();
		
		CTAStation[] CTAcollection= new CTAStation[count];
		Scanner input2 = new Scanner(new File("C:\\Users\\mario\\git\\cs201-202201-mcerecer\\src\\labs\\lab5\\CTAStops.csv"));
		
		count=0;
		while(input2.hasNextLine()) {
			boolean wheel = false;
			boolean open = false;
			String data = input2.nextLine();
			String[] data2 = data.split(",");
			
			for (int i=0; i<data2.length; i++ ) {
				System.out.println(data2[i]);	
			}
			
			double lat = Double.parseDouble(data2[1]);
			double lng = Double.parseDouble(data2[2]);
		
			if(data2[4].equalsIgnoreCase("true")) {
				wheel = true;
			}else if (data2[5].equalsIgnoreCase("true")){
				open = true;
			}
			CTAStation c = new CTAStation(data2[0], lat, lng, data2[3], wheel ,open);
			CTAcollection[count]=c;
			count++;
		}
		input2.close();
		
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return CTAcollection;

	}
	
	public static void menu(CTAStation[] Stops){
		
	Scanner input3 = new Scanner(System.in);
	
	String choice = "xyz";

		
		while(!(choice.equalsIgnoreCase("4"))) {

			System.out.println("(1)Display Station Names");
			System.out.println("(2)Display Stations with/without Wheelchair access");
			System.out.println("(3)Display Nearest Station");
			System.out.println("(4)Exit");
			
			choice = input3.nextLine();
			
			input3.close();
			
			switch (choice){
			
			case "1":
			for (int k=0; k < Stops.length; k++) {
				System.out.println(Stops[k].getName());
			}
			case "2":
			Scanner input4 = new Scanner(System.in);
			System.out.println("Select 'y' or 'n'");
			
			String choice2 = input4.nextLine();
			
			while (!(choice2.equalsIgnoreCase("y")) && !(choice2.equalsIgnoreCase("n"))) {
				System.out.println("Select 'y' or 'n'");
				choice2 = input4.nextLine();
			};
			
			for(int j=0; j < Stops.length; j++) 
			{
			if(choice2.equalsIgnoreCase("y")) {
				if(Stops[j].hasWheelchair()==true) {
					System.out.println(Stops[j].getName());
				}
			}else if(choice2.equalsIgnoreCase("n")) {
				if(Stops[j].hasWheelchair()==false) {
					System.out.println(Stops[j].getName());
				}
			}
			}
			case "3":
				
			double[] lcl= new double[Stops.length];
			Scanner input5 = new Scanner(System.in);
			String lat = input5.nextLine();
			System.out.println("Enter lat");
			String lng = input5.nextLine();
			System.out.println("Enter lng");
			double lat2 = Double.parseDouble(lat);
			double lng2 = Double.parseDouble(lng);
			for(int p = 0; p<Stops.length; p++) {
				lcl[p]=Math.abs(CTAcollection[p].calcDistance(lat2,lng2)-CTAcollection[p].calcDistance(CTAcollection[p].getLat(),CTAcollection[p].getLng()));
			}
			double minval=lcl[0];
			int idx=0;
			for(int q = 0; q<Stops.length; q++) {
				if (minval<lcl[q]) {
					minval=lcl[q];
					idx=q;
			}
			System.out.println(CTAcollection[idx].getName());
			}
		
		}
	}
}
}