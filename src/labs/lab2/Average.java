package labs.lab2;
//Mario Cerecer
//CS201
//Section 1
//01-31-2021
//Average
//This programs prompts the user for grades and calculate the average until "-1" is introduced
import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		
		double g = 0;//create variables
		double x=0;
		int c=0;
		Scanner input = new Scanner(System.in);//open scanner
		
		System.out.print("Enter a grade ");//ask for grades
		
		g = Integer.parseInt(input.nextLine());//enter grades
		
		while (g != -1) {//Condition for loop
			x=x+g;//Addition of grades
			c=c+1;//Number of grades entered
			double av= x/c;//Calculate average
			
			System.out.println("The average is "+av);
			
			System.out.print("Enter a grade ");//Ask for the next grade
			
			g = Integer.parseInt(input.nextLine());//enter grades
		}
		input.close();//close scanner
	}

}
