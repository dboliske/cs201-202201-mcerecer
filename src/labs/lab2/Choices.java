package labs.lab2;
//Mario Cerecer
//CS201
//Section 1
//01-31-2021
//Square
//This programs display a menu of choices and prompts the user to select one. Depending on the choice selected, it will either say "Hello", do an addition operation,
//do a multiplication operation, or exit the program
import java.util.Scanner;

public class Choices {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Choice one of the next options ");//open scanner
		
		System.out.println("Hello");//show options
		System.out.println("Addition");
		System.out.println("Multiplication");
		System.out.println("Exit");
		
		String choice = input.nextLine();//input choice
		
		while (!choice.equals("Exit")) {//loop condition
			if (choice.equals("Hello")) {// case "Hello"
				System.out.println("-Hello-");//output
				
				System.out.println("Choice one of the next options ");//show options
				
				System.out.println("Hello");
				System.out.println("Addition");
				System.out.println("Multiplication");
				System.out.println("Exit");
				
				choice = input.nextLine();//input choice
				
			}else if (choice.equals("Addition")){//case "Addition"
				System.out.println("Enter first value");
				int x = Integer.parseInt(input.nextLine());//create x variable
				System.out.println("Enter second value");
				int y = Integer.parseInt(input.nextLine());//create y variable
				
				int z= x+y;//arithmetic
				System.out.println(z);//output
				
				System.out.println("Choice one of the next options ");//show options
				
				System.out.println("Hello");
				System.out.println("Addition");
				System.out.println("Multiplication");
				System.out.println("Exit");
				
				choice = input.nextLine();//input choice
				
			}else if (choice.equals("Multiplication")) {//case "Multiplication"
				System.out.println("Enter first value");
				int x2 = Integer.parseInt(input.nextLine());//create variable x2
				System.out.println("Enter second value");
				int y2 = Integer.parseInt(input.nextLine());//create variable y2
				
				int z2= x2*y2;//calculate average
				System.out.println(z2);//output
				
				System.out.println("Choice one of the next options ");//show options
				
				System.out.println("Hello");
				System.out.println("Addittion");
				System.out.println("Multiplication");
				System.out.println("Exit");
				
				choice = input.nextLine();//input choice
			}else {//Incorrect input case
				System.out.println("Choice one of the next options ");
				
				System.out.println("Hello");
				System.out.println("Addittion");
				System.out.println("Multiplication");
				System.out.println("Exit");
				
				choice = input.nextLine();
			}
		}
	}
}
